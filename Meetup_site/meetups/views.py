from django.shortcuts import render
import meetups  
# Create your views here.
def index(request):
    meetups = [
        {'title': 'A First Meetup',
        'location':'Paris',
        'slug':'a-first-meetup'},
        {'title': 'A Second Meetup',
        'location':'USA',
        'slug':'a-second-meetup'}
    ]
    return render(request,'meetups/index.html',
    {'show_meetups':True,
    'meetups':meetups})

def meetup_details(request,meetup_slug):
    selected_meetup ={'title':'A first Meetup',
    'description':'This is the first meetup'
    }

    return render(request,'meetups/meetup-details.html',
    {'meetup_title':selected_meetup['title'],
    'meetup_description':selected_meetup['description']})

    