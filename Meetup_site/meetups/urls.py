from unicodedata import name
from django.urls import path
from . import views
#Creating a url configuration
urlpatterns = [
  path('meetups/',views.index), #www.domain-name.com
#path('meetups/<slug:meetup_slug>',views.meetup_details),
  path('meetups/<slug:meetup_slug>',views.meetup_details,name='meetup-details'),
]
#note:it is important to add a forward slash to the app ,incase a user adds a forward slash to the site we avoid 404 error